# FSC OPA integration

This repo is an example of how to use [Open Policy Agent (OPA)](https://www.openpolicyagent.org) for policy-based authorization in combination with your FSC inway and/or outway.

## Step 1. Deploy OPA
Deploy an OPA instance on your Kubernetes cluster. Please find the manifests for this in the `opa` directory.

Make sure you have a certificate for your OPA instance, because FSC requires OPA to use HTTPS. This example uses Cert-Manager to create a certificate using the FSC CA.

Second, make sure you have some valid Rego policies. This example uses the `inway.rego` file in the `opa/rego` directory. You may also add some data in the respective directory.

```bash
kubectl apply -k opa
```

## Step 2. Configure FSC to use OPA
The FSC inway and outway Helm charts have built-in support to validate a request or response through OPA. Enable it like this:

```yaml
config:
  # ...
  authorizationService:
    enabled: true
    url: https://opa-svc/v1/data/httpapi/inway/allow
  # ...
```

The OPA URL uses the following structure:

`https://<opa-service-name>/v1/data/<package-namespace>/<policy-name>`

The package namespace is the namespace of the Rego policy file (see the first line of the file).

## Need help building your own Rego policies?
The Rego syntax is quite complex. You can use the [Rego policy builder](https://gitlab.com/digilab.overheid.nl/platform/rego-policy-builder) to help you build your own policies using a UI / code generator.
